import re
import sys
import json
import operator

import numpy as np
import matplotlib.pyplot as plt

first = operator.itemgetter(0)
snd = operator.itemgetter(1)

def figure_5(*fnames):
    pct_ids = np.array([float(re.search(r'([\d.]+)_id/', n).group(1)) for
                        n in fnames])
    fig, (sp_ax, nonsp_ax) = plt.subplots(2, sharex=True, sharey=True)
    sp_ax.set_title("Percentage of correct spurious classifications")
    nonsp_ax.set_title("Percentage of correct non-spurious classifications")
    x = None
    for pct_id, fname in zip(pct_ids, fnames):
        with open(fname) as f:
            data = json.load(f)
        if x is None:
            x = 1 - 2*np.array(data['ci_widths'])
        labels, noexc = zip(*[(l,c) for l, c in data['data'] if len(c) > 1])
        nonspur_idxs, spur_idxs = list(), list()
        for i, label in enumerate(labels):
            if '-from-' in label:
                spur_idxs.append(i)
            else:
                nonspur_idxs.append(i)
        calls = np.array(list(noexc))
        sp_tpr = np.mean(calls[spur_idxs], axis=0)
        sp_ax.plot(x, sp_tpr*100, label=r'%i %%ID'%(pct_id))
        nonsp_tpr = np.mean(~calls[nonspur_idxs], axis=0)
        nonsp_ax.plot(x, nonsp_tpr*100)

    plt.ylabel("Percent correct")
    plt.xlabel("Credible interval width")
    sp_ax.legend(loc="lower left", ncol=2)
    plt.show()


def figure_4(*fnames):
    nus = np.array([float(re.search(r'_([\d.]+)_', n).group(1)) for n in fnames])
    fig, (sp_ax, nonsp_ax) = plt.subplots(2, sharex=True, sharey=True)
    sp_ax.set_title("Percentage of correct spurious classifications")
    nonsp_ax.set_title("Percentage of correct non-spurious classifications")
    x = None
    for nu, fname in zip(nus, fnames):
        with open(fname) as f:
            data = json.load(f)
        if x is None:
            x = 1 - 2*np.array(data['ci_widths'])
        labels, noexc = zip(*[(l,c) for l, c in data['data'] if len(c) > 1])
        nonspur_idxs, spur_idxs = list(), list()
        for i, label in enumerate(labels):
            if '-from-' in label:
                spur_idxs.append(i)
            else:
                nonspur_idxs.append(i)
        calls = np.array(list(noexc))
        sp_tpr = np.mean(calls[spur_idxs], axis=0)
        sp_ax.plot(x, sp_tpr*100, label=r'$\nu$=%1.1f'%(nu))
        nonsp_tpr = np.mean(~calls[nonspur_idxs], axis=0)
        nonsp_ax.plot(x, nonsp_tpr*100)

    plt.ylabel("Percent correct")
    plt.xlabel("Credible interval width")
    sp_ax.legend(loc="lower left")
    plt.show()
    

if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
