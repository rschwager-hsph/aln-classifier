import os
import re
import sys
import json
from itertools import chain
from collections import Counter
from collections import namedtuple

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from toolz import groupby

SearchResult = namedtuple("SearchResult",
                          "read_id idx_id start read_length idx_length")

def transform(func, d):
    return dict([(k, func(v)) for k, v in d.iteritems()])

def parse_results(f):
    for line in f:
        if line.startswith("@"):
            continue
        fields = line.strip().split('\t')
        rec = SearchResult(fields[0], fields[2], int(fields[3])-1,
                           len(fields[9]), int(fields[13].split(":")[-1]))

        yield rec

    
def plot_dist_empcl(ax, data, note=" ", **kwargs):
    bins, edges = np.histogram(data, **kwargs)
    ax.bar(edges[:-1], bins/float(len(data)), width=np.diff(edges))
    ax.set_ylabel("Density")
    mx, mean = bins.max(), data.mean()
    ax.arrow(edges[np.argmax(bins)],
             mx, 0, -mx, color='r', linestyle="dotted", linewidth=3.0)
    ax.arrow(edges[np.argmin(np.abs(edges-mean))],
             mean, 0, -mx*0.9, color='g', linestyle="solid")
    ax.set_xlabel(note+"mode=%.3f, mean=%.3f, n=%i"%(
        edges[np.argmax(bins)], mean, np.sum(data)))
    return ax


def parse_contaminated(idx_id):
    packed = re.match(r'(.+)\[\d+:\d+\]-from-(.+)\[(\d+):(\d+)\]$',
                      idx_id).groups()
    base, contaminant, beg, end = packed
    return base, contaminant, int(beg), int(end)

result_types = ["hit", "miss", "spurious_hit", "spurious_miss"]
rcolors = dict(hit="b", miss="r", spurious_hit="g", spurious_miss="m")

def _classify_simple(correct_answer, answer, hit, miss):
    if correct_answer == answer:
        return hit # hit
    else:
        return miss # miss

def overlap(a_coords, b_coords):
    betw_a = lambda v: a_coords[0] < v < a_coords[1]
    betw_b = lambda v: b_coords[0] < v < b_coords[1]
    bot, top = min(a_coords[0], b_coords[0]), max(a_coords[1], b_coords[1])
    s = sum( betw_a(i) and betw_b(i) for i in range(bot, top) )
    return float(s)/(top-bot)
    
def classify_searchresult(res):
    correct_answer = res.read_id.split("|")[-1]
    if '-from-' not in res.idx_id:
        return _classify_simple(correct_answer, res.idx_id, *result_types[:2])
    base, contaminant, beg, end = parse_contaminated(res.idx_id)
    if overlap((beg, end), (res.start, res.start+res.read_length)) >= 0.5:
        # aligned to a contaminated region
        return _classify_simple(correct_answer, contaminant, *result_types[2:])
    else: # aligned to a noncontaminated region
        return _classify_simple(correct_answer, base, *result_types[:2])
        

def gene_name(ress):
    hits, spur_hits = ress.get("hit", []), ress.get("spurious_hit", [])
    collection = max(hits, spur_hits, key=len)
    cnt = Counter([r.idx_id for r in collection]).most_common()
    if cnt:
        return cnt[0][0]
    else:
        return "Unknown"


def plot_startsite(grouped, res_sizes, output_dir):
    n_cats = len(grouped.keys())
    spec = GridSpec(3+n_cats, 3)
    for i, (rtype, ress) in enumerate(grouped.iteritems(), 3):
        ax = plt.subplot(spec[i,:])
        h,b = np.histogram([float(r.start)/r.idx_length for r in ress],
                           bins=max(int(np.sqrt(len(ress))), 10))
        ax.set_ylabel(rtype)
        ax.scatter(b[:-1], h/float(len(ress)), c=rcolors[rtype],
                   marker='.')
    bigax = plt.subplot(spec[:3,:])
    allstarts = [float(r.start)/r.idx_length
                 for r in chain.from_iterable(grouped.itervalues())]
    h,b = np.histogram(allstarts, bins=max(int(np.sqrt(len(ress))), 10))
    bigax.scatter(b[:-1],  h/float(len(ress)), c='k', marker='.', label="Total")
    bigax.set_ylabel("Total")
    plt.xlim((0.,1.))
    _title = " ".join(["%s=%i"%(t, sz) for t, sz in res_sizes.iteritems()])
    fbase = "_".join(["%s-%i"%(t, sz) for t, sz in res_sizes.iteritems()])
    gname = gene_name(grouped)
    plt.title(gname+" "+_title)
    plt.tight_layout()
    plt.gcf().set_size_inches(8.5, 11)
    out = os.path.join(output_dir, gname+"_"+fbase+".png")
    print "Saving startsite to "+out
    plt.savefig(out, dpi=120)
    plt.close()


def save_json(grouped, res_sizes, output_dir):
    stats = "_".join(["%s-%i"%(t, sz) for t, sz in res_sizes.iteritems()])
    fname = os.path.join(output_dir, gene_name(grouped)+"_"+stats+".json")
    with open(fname, 'w') as f:
        json.dump({"data": grouped}, f)


def coverages(results):
    """Compute the list of alignment coverage for all sites

    :param results: all search results for one gene"""
    gene_len = results[0].idx_length
    cov = np.zeros(gene_len)
    for r in results:
        start = r.start-1
        stop = start+r.read_length
        cov[start:stop] += 1
    return cov


def plot_covhist(ress, res_sizes, output_dir):
    xformd = transform(coverages, ress)
    n_cats = len(xformd.keys())
    spec = GridSpec(3+n_cats, 3)
    for i, (rtype, sitecov) in enumerate(xformd.iteritems(), 3):
        ax = plt.subplot(spec[i,:])
        plot_dist_empcl(ax, sitecov, note=rtype+" ", bins=np.sqrt(len(sitecov)))
    bigax = plt.subplot(spec[:3,:])
    total_cov = np.sum(xformd.values(), axis=0)
    plot_dist_empcl(bigax, total_cov, 
                    note="total ", bins=np.sqrt(len(total_cov)))
    _title = " ".join(["%s=%i"%(t, sz) for t, sz in res_sizes.iteritems()])
    fbase = "_".join(["%s-%i"%(t, sz) for t, sz in res_sizes.iteritems()])
    gname = gene_name(ress)
    plt.title(gname+" "+_title)
    plt.tight_layout()
    plt.gcf().set_size_inches(8.5,11)
    out = os.path.join(output_dir, gname+"_"+fbase+".png")
    print "Saving covhist to "+out
    plt.savefig(out, dpi=120)
    plt.close()
    

def digest(startsite_output_dir, json_output_dir, covhist_output_dir, ress):
    grouped = groupby(classify_searchresult, ress)
    res_sizes = dict([(rtype, len(grouped.get(rtype, [])))
                      for rtype in result_types])
    plot_startsite(grouped, res_sizes, startsite_output_dir)
    plot_covhist(grouped, res_sizes, covhist_output_dir)
    save_json(grouped, res_sizes, json_output_dir)

def base_hit(result):
    i = result.idx_id
    if '[' in i:
        return i.split('[', 1)[0]
    return i

def main(to_parse, startsite_output_dir, json_output_dir, covhist_output_dir):
    with open(to_parse, 'r') as f:
        ress = parse_results(f)
        idx = groupby(base_hit, ress) # group by idx_id
    for idx_id, ress in idx.iteritems():
        if len(ress) < 10:
            continue
        digest(startsite_output_dir, json_output_dir, covhist_output_dir, ress)

if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
