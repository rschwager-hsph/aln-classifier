+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
A Bayesian Classifier for Improved Metagenomic Functional Profiling
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Randall Schwager

Introduction
============

Among the many applications of contemporary, high-throughput DNA and
RNA sequencing technologies is the characterization of bacterial,
viral, metazoan, and archaeal communities living on and in humans
[#hmp]_.  Sequencing the genomes present in these communities
illuminates a metagenome that varies widely in functional and
phylogenic diversity among skin, gut, mouth, airway, and urogenital
habitats [#hmpA]_.  Microbiome composition is an important factor in
host metabolism [#kwash]_ [#fat]_, a variety of infectious diseases
[#infl]_, and host immune response [#ibd]_.

Whole metagenome shotgun (WMS) sequencing provides an unprecedented
amount of information on a habitat’s microbiome.  WMS data sets of the
human gut generally contain ~150 million base pairs (Mbp) per sample
and recent metagenomic studies have sequenced over 100 samples
[#metarep]_.  A host of metagenomic analysis tools have emerged over
the past several years to provide deep insight into microbial
communities [#toolrev]_.

The best explored area of metagenomic sequence analysis is that of
taxonomic profiling.  Many taxonomic profiling tools are classifiers
that assign WMS reads to taxonomic units in an effort to answer the
question of “who’s there”.  A less explored but critically important
aspect of metagenomic sequence analysis is that of functional
profiling, an approach where reads are mapped against annotated
genomes to determine the metabolic pathways present in a sample by way
of inferring pathways from proteins, and proteins from genes.
Functional profiling answers the question, “what’s happening?”
Although multiple functional profiling tools exist for metagenomic
data, the current top performers for metabolic and functional
reconstruction of metagenomic communities is the HMP Unified Metabolic
Analysis Network (HUMAnN) [#humann]_ and its successor HUMAnN2 [#humann2]_.

HUMAnN2 is a pipeline that uses a set of existing and novel tools in a
metagenomically-tuned fashion. One of the core steps in HUMAnN is
mapping short nucleotide reads to annotated protein families. This
procedure provides the input for metabolic pathway construction by
principle of maximum parsimony with MinPath [#minpath]_.

A combination of short nucleotide reads, protein motifs, and
low-complexity sequences create spurious read-to-protein
mappings. Although maximum parsimony guards against formation of
spurious metabolic pathways, a greater number of genes available for
constructing pathways increases the rate of spurious pathways
constructed. Thus, one way to cut down on spurious pathway formation
and improve HUMAnN2 accuracy is to restrict the set of available genes
for pathway reconstruction to only those genes we are certain to
exist.

Methods
=======

Read-to-protein mappings are composed of an alignment start site, read
(translated to protein sequences) length, and the length of the
protein sequence to which the read mapped. When normalized by target
sequence length, spurious mapping start site frequencies form a
pattern resembling a rectangle function while true mapping start site
frequencies are uniformly distributed across the length of the
gene. These alignment start site patterns can be modeled by a Beta
distribution, with uniform start site frequencies represented by
:math:`B(1,1)` and nonuniform start site frequencies significantly
different from :math:`B(1,1)`. An example spurious and nonspurious
alignment pattern, along with Beta distribution models, are plotted in
Figure 1.

.. figure:: figure_1.png

	    **Figure 1**

	    Example simulated spurious and nonspurious alignment start
	    site frequencies. Simulated spurious alignments are
	    plotted on the left, while simulated nonspurious
	    alignments are plotted on the right. Beta models of these
	    alignment patterns are plotted in blue. :math:`\alpha` and
	    :math:`\beta` for the models were estimated using maximum
	    likelihood.
	    

Such a model lacks the ability to incorporate prior knowledge of the
probability of spurious gene mapping into the model. To incorporate
prior information into the model, I introduce the following model:

.. math::

   RF \mid \alpha, \beta \sim\ \ \text{Beta}(\alpha, \beta)

   \alpha \sim \text{Gamma}(\nu, \nu)

   \beta \sim \text{Gamma}(\nu, \nu)

where :math:`RF` is the read alignment start site.

This model incoporates prior information varying a single paramater
:math:`\nu`. By parametrizing the Gamma distribution such that the
mean :math:`\frac{\nu}{\nu}` is always equal to one, varying
:math:`\nu` simply changes the amount of belief of how common
uniformly distributed, and thus nonspurious, gene alignment patterns
occur.

The application of this model is binary classification, binning genes
into "spurious" or "nonspurious" categories depending on mapping start
site patterns. The application allows a input of prior information
from a second source by setting what range around :math:`\alpha=1` and
:math:`\beta=1` is considered uniform. Conference with the HUMAnN2
development team determined that the acceptable range of alpha and
beta were :math:`\alpha \in (0.9,1.1)` and :math:`\beta \in
(0.9,1.1)`, with :math:`\nu = 0.2`.

To test the efficacy of this model for classification, I simulated
data using the following procedure: The most recent release of UniRef
annotated protein sequence clusters (50% similarity) were downloaded
[#uniref]_. Three random but mututally distinct subsets of roughly
3,500 UniRef50 sequences were selected : :math:`A`, :math:`B`, and
:math:`C`. Sequences from subset :math:`A` were contaminated with
sequences from subset :math:`C` producing subset :math:`A\prime`. To
contaminate a sequence, a contamination start site was selected at
random according to a uniform distribution. A contamination stop site
was selected 20 to 33 positions away from the start site according to
a uniform distribution. Sequence sets :math:`B` and :math:`A\prime`
were concatenated to produce the model gene database. Sequence sets
:math:`B` and :math:`C` were concatenated to produce the model
metagenome. Reads of 100nt length were simulated from the model
metagenome by randomly selecting a proteinfrom the metagenome,
randomly choosing a translation reading frame, and reverse translating
the protein sequence. Mutations of varying percent identity were
induced into the reads by using the BLOSUM62 substitution matrix .
Finally, reads were mapped against the model gene database using the
translated alignment application, DIAMOND [#diamond]_.

The simulation procedure produced 11 sets of 500,000 reads each, where
each dataset was generated from the :math:`B+C`. The 11 read sets
varied in mutation identity, ranging in 5% intervals from 50% identity
to 100% identity to the original simulated metagenome. The read sets
produced an average of 240,000 mapping results per set, with a maximum
of 270,000 results from the 100% identity read set and a minimum of
146,000 results from the 50% identity read set. Mapping results were
aggregated by target gene and fed to the classifier. Classification
results were compared to the true read-target mapping relationship to
determine classification validity.

Genes were classified as being spurious or nonspurious using the model
outlined above. Since a typical translated mapping run can generate
alignments to thousands of genes, the classifier must be
time-efficient. A number of optimizations were used to improve
wall-clock and CPU time efficiency. The posterior mode was found with
the BFGS quasi-Newton optimization algorithm. This amounted to
maximizing the joint probability

.. math::

   \Pr(\alpha, \beta \mid \mathbf{y}) \propto
   \  -n \log \text{B}(\alpha, \beta)
      + (\alpha-1)\log \sum_{i=1}^{n} \mathbf{y}_i
      + (\beta-1)\log \sum_{i=1}^{n} (1-\mathbf{y}_i)
      + (\nu-1)\log \alpha\beta - \nu(\alpha+\beta)

where :math:`\mathbf{y}` is the length :math:`n` vector of read
mapping start sites and :math:`\text{B}(\alpha, \beta)` is the beta
function at :math:`\alpha` and :math:`\beta`. A closed-form posterior
distribution was difficult to obtain. The logistics of using MCMC
methods to sample from the posterior would prove impractical for
thousands of independent posteriors; assessing convergence of MCMC
chains is an as-yet imperfect and computationally expensive
science. Mercifully, the model requires estimation of only two
parameters per gene, making calculation of :math:`\Pr(\mathbf{y})` by
quadrature the most accurate and efficient option for sampling from
the posterior.  The variance of the normal approximation of the
posterior was used to automatically estimate limits for quadrature
over :math:`\alpha` and :math:`\beta`. I used 2.5 times the variance
of the normal approximation, equivalent to

.. math::

   \text{Range}_\beta = \pm Q\ S_{\alpha \alpha} 

   \text{Range}_\alpha = \pm Q\ S_{\beta \beta}

where

.. math::

   Q = \frac{-2.5}{S_{\alpha \alpha} S_{\beta \beta} - 2 S_{\beta \alpha}}
   
   S_{\alpha \alpha} = n(\psi_{1}(\alpha+\beta) - \psi_{1}(\alpha))

   S_{\beta \beta} = n(\psi_{1}(\alpha+\beta) - \psi_{1}(\beta))

   S_{\beta \alpha} = S_{\alpha\beta} = n(\psi_{1}(\alpha+\beta))

and :math:`n` is the number of translation start sites, the values of
:math:`\alpha` :math:`\beta` are those found at the posterior mode and
:math:`\psi_{1}` is the trigamma function, or the second derivative of
the log of the Gamma function. The variance of the prior distribution
:math:`1/\nu` was used in cases where it was larger than variance of
the normal approximation.

50 marginal posterior credibility regions of linearly increasing width
were then calculated. If either the marginal posterior region for
:math:`\alpha` or the marginal posterior region for :math:`\beta`
overlapped with :math:`(0.9, 1.1)`, the gene was called as
spurious. If the integration limits around :math:`\alpha` or
:math:`\beta` did not contain 1, the gene was immediately called as
spurious for all credibility regions.



Results
=======

The posterior :math:`\Pr(\alpha, \beta \mid \mathbf{y})` surface
appears to be convex and unimodal, as seen in an example contour in
Figure 2. Of the 61,226 gene classifications performed in this
evaluation, 21 classifications failed due to being unable to find a
posterior mode within 400 BFGS iterations.

.. figure:: figure_2.png
	    :width: 850px

	    **Figure 2**

	    A selected posterior surface. The density of
	    :math:`\alpha` is plotted on the X-axis, while the density
	    of :math:`\beta` is plotted on the Y-axis. For this
	    posterior, the value for :math:`\nu` was 6.

The effect of the prior parameter :math:`\nu` on the model is shown in
Figure 3. We can see that as nu is increased, the models' prediction
of mapping start site frequency regresses to uniform. The effect of
prior parameter is tempered by the amount of reads :math:`n` and the
amount of uniformity in the likelihood: only poorly informed and
non-uniform mapping patterns are made significantly more uniform.

.. figure:: figure_3.png
	    :width: 850px

	    **Figure 3**

	    Example simulated spurious and nonspurious alignment start
	    site frequencies with density modeled by the Beta-Gamma
	    model. Each line plots the density of a Beta distribution
	    parametrized by :math:`\alpha` and :math:`\beta` at the
	    posterior mode. Increasing values of :math:`\nu` are
	    plotted as different colored lines, with the smallest
	    value for :math:`\nu` in blue and the largest value for
	    :math:`\nu` in magenta.

To gauge the overall effectiveness of the classifier, the percentage
of correct classifications are plotted in figures 4 and 5. Figure 4
gauges the sensitivity of the classifier to different values of prior
parameter :math:`\nu`, while Figure 5 gauges the effect of mutation
percent identity on classification accuracy.

.. figure:: figure_4.png
	    :width: 850px

	    **Figure 4**

	    Percentage of correct spurious and nonspurious
	    classifications as a function of credible interval width
	    and prior parameter :math:`\nu` and mutation percent
	    identity set to 100%.


.. figure:: figure_5.png
	    :width: 850px

	    **Figure 5**

	    Percentage of correct spurious and nonspurious
	    classifications as a function of credible interval width
	    and mutation percent identity and :math:`\nu` set to 0.1.
		  

Overall runtime statistics are a function of the number of genes to
classify. Of the 61,226 gene classifications performed in this
evaluation, the overall rate of classification was 565 genes per CPU
core per minute. Runtime can be decreased by having less sparsely
aligned reads: higher read numbers increase :math:`n`, thereby
decreasing the integration range. If the integration range of both
:math:`\alpha` and :math:`\beta` falls outside of :math:`(1,1)`, the
posterior is not fully computed. In situations where numerical
integration is not attempted, the classification speed increases from
300-400ms per classification to 10-30ms per classification.

All code, documents, and figures, along with instructions on how to
replicate this evaluation are available on bitbucket.org at
https://bitbucket.org/rschwager-hsph/aln-classifier.

Conclusions
===========

Overall, the classifier seems to be an acceptable first attempt at
classifying spurious gene alignments. At its most accurate, the
classifier is able to correctly classify 80% of spurious alignments,
while at the same time correctly classifying 94% of nonspurious
alignments. The most accurate prior parameter setting for this
simulated dataset were at a farily noninformative :math:`\nu=0.1`,
meaning that the expected difference of the likelihood paramters
:math:`\alpha` and :math:`\beta` from 1 was :math:`\sqrt{10} \approx 3.16`.


Future Work
===========

The marginal credible intervals of :math:`\Pr(\alpha \mid \mathbf{y})`
and :math:`\Pr(\beta \mid \mathbf{y})` could be used more
effictively. Using them to directly asses the uniformity of data they
would produce in a Beta distribution would likely improve accuracy. As
is, the model treats :math:`(0.9,1.1)` as the truth for uniformity;
expanding this range will likely introduce the same sensitivity /
specificity tradeoff as with varying :math:`\nu`. A step in the right
direction would be to survey the correlation between :math:`\Pr(\alpha
\mid \mathbf{y})` and :math:`\Pr(\beta \mid \mathbf{y})`; if the
correlation is high, the parameters rarely disagree and a single
threshold could be used.

One aspect of the spurious alignment problem that this mdoel
completely ignores is that genes may have a mixture of true and
spurious mapping results. A finite mixture of beta distributions may
be a good option for detecting and handling these scenarios: the least
uniform beta distributions can be subtracted from the mapping results,
thus repairing rather than discarding spurious mapping results for a gene.

References
==========

.. [#hmp] The Human Microbiome Consortium. (2012) A framework for human
       microbiome research. Nature 486: 215–221.

.. [#hmpA] The Human Microbiome Project Consortium. (2012) Structure,
       function and diversity of the healthy human microbiome. Nature
       486: 207–214.

.. [#kwash] Smith MI, Yatsunenko T, Manary MJ, Trehan I, Mkakosya R, Cheng
       J, Kau AL, Rich SS, Concannon P, Mychaleckyj JC, Liu J, Houpt
       E, Li JV, Holmes E, Nicholson J, Knights D, Ursell LK, Knight
       R, Gordon JI. (2013) Gut microbiomes of Malawian twin pairs
       discordant for kwashiorkor. Science. 336:548-54. doi:
       10.1126/science.1229000.

.. [#fat] Le Chatelier E, Nielsen T, Qin J, et al. (2013) Richness of
       human gut microbiome correlates with metabolic markers. Nature
       500: 541–546 doi:10.1038/nature12506.

.. [#infl] Garrett WS, Gordon JI, Glimcher LH (2010) Review Homeostasis
       and inflammation in the intestine. Cell 140: 859–870.

.. [#ibd] Morgan XC, Tickle TL, Sokol H, Gevers D, Devaney KL, Ward DV,
       Reyes JA, Shah SA, Leleiko N, Snapper SB, Bousvaros A, Korzenik
       J, Sands BE, Xavier RJ, Huttenhower C (2012) Dysfunction of the
       intestinal microbiome in inflammatory bowel disease and
       treatment. Genome Biol 13: R79.

.. [#metarep] Qin J, Li R, Raes J, Arumugam M, Burgdorf KS, et al. (2010) A
       human gut microbial gene catalogue established by metagenomic
       sequencing. Nature 464: 59–65.

.. [#toolrev] Segata N, Boernigen D, Tickle T, Morgan XC, Garrett WS,
       Huttenhower C. (2013) Review Computational meta’omics for
       microbial community studies. Molecular Systems Biology 9:666.

.. [#humann] Abubucker S, Segata N, Goll J, Schubert AM, Izard J, et
       al. (2012) Metabolic Reconstruction for Metagenomic Data and
       Its Application to the Human Microbiome. PLoS Comput Biol 8:
       e1002358.

.. [#humann2] http://huttenhower.sph.harvard.edu/humann2

.. [#minpath] Ye Y, Doak TG. (2009) A parsimony approach to biological
       pathway reconstruction/inference for genomes and
       metagenomes. PLoS Comput Biol. 5:e1000465.1.

.. [#uniref] Suzek BE, Wang Y, Huang H, McGarvey PB, Wu CH, UniProt
       Consortium. (2015) UniRef clusters: a comprehensive and
       scalable alternative for improving sequence similarity
       searches. Bioinformatics 2015 Mar 15;31(6).

.. [#diamond] Buchfink B, Xie C, Huson DH. (2015) Fast and Sensitive
       Protein Alignment using DIAMOND. Nature Methods 12, 59–60.
       doi:10.1038/nmeth.3176.

