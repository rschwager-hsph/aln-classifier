import os
import sys
import json
from collections import namedtuple
from itertools import chain
from multiprocessing import Pool

import numpy as np
from scipy.special import betaln
from scipy.special import polygamma
from scipy.special import psi
from scipy.stats import gamma
from scipy.optimize import fmin_bfgs

nugamma = lambda nu: gamma(nu, scale=1./nu)

def startsites(data_fname):
    with open(data_fname, 'r') as f:
        data = json.load(f)
    return np.array([
        float(l[2]+1)/l[-1] for l in
        chain.from_iterable(data['data'].itervalues())
    ])
    

reallyunlikely = np.finfo(float).min
def ljoint(alpha, beta, nu, n, lp, lq, check_undefined=False):
    """product of iid Beta(alpha,beta) likelihood and two ind draws from
    gamma(nu,nu)

    :param nu: prior variance
    :param n: number of start sites
    :param lp: sum(log(start_sites))
    :param lq: sum(log(1-start_sites))

    """
    if check_undefined and (alpha < 0 or beta < 0):
        return reallyunlikely # effectively impossible
    return -n*betaln(alpha,beta) + ( (alpha-1)*lp + (beta-1)*lq ) \
        + (nu-1)*np.log(alpha*beta) - nu*(alpha+beta)


def ljointgrad(alpha, beta, nu, n, lp, lq, check_undefined=False):
    a, b = float(alpha), float(beta)
    psi_ab = psi(a+b)
    da = lp + (nu-1)/a - n*(psi(a) - psi_ab) - nu
    db = lq + (nu-1)/b - n*(psi(b) - psi_ab) - nu
    return np.array([da, db])


SufficientStatistics = namedtuple("SufficientStatistics", ["n", "lp", "lq"])
def suff_stats(data):
    return SufficientStatistics(
        len(data), np.log(data).sum(), np.log(1-data).sum()
    )


def mle_var(a, b, n):
    """return the inverse fisher information matrix of the beta likelihood
    evaluated at (a,b)

    """

    tg_ab = polygamma(1, a+b)
    aa = n*(tg_ab - polygamma(1, a))
    ab_ba = n*tg_ab
    bb = n*(tg_ab - polygamma(1, b))
    ret = np.array([[    bb, -ab_ba],
                    [-ab_ba, aa]])
    return -1./(aa*bb - ab_ba**2)*ret


def post_mode(sufficient_statistics, nu):
    n, lp, lq = sufficient_statistics
    ret = fmin_bfgs(lambda arr: -ljoint(arr[0], arr[1], nu, n, lp, lq, True),
                    np.array([1,1]),
                    lambda arr: -ljointgrad(arr[0], arr[1], nu, n, lp, lq),
                    disp=False, full_output=True)
    if ret[-1]:
        raise Exception("Unable to find posterior mode")
    return ret[0]

    
eps = np.finfo(float).eps
def grid_limits(a, b, n, nu):
    I = mle_var(a, b, n)
    var_min = 1./nu # prior variance
    a_var, b_var = max(var_min, I[0,0]), max(var_min, I[1,1])
    ll_a, ul_a = a-2*a_var, a+2*a_var
    ll_b, ul_b = b-2*b_var, b+2*b_var
    return max(eps, ll_a), ul_a, max(eps, ll_b), ul_b
    

def post_grid(grid_bounds, sufficient_statistics, nu, size):
    n, lp, lq = sufficient_statistics
    ll_a, ul_a, ll_b, ul_b = grid_bounds
    alpha, beta = np.linspace(ll_a, ul_a, size), np.linspace(ll_a, ul_b, size)
    grid = np.empty((size,size)) # alpha varied by x, beta varied by y
    for i, a in enumerate(alpha):
        grid[i] = ljoint(a, beta, nu, n, lp, lq)
    return alpha, beta, np.exp(grid - np.log(np.exp(grid).sum()))
    

def post_sample(alpha, beta, grid, size=1000):
    ret = np.empty((size,2))
    marg_b = grid.sum(axis=0)
    marg_a = grid.sum(axis=1)
    rng = np.arange(alpha.shape[0])
    b_idxs = np.random.choice(rng, size=size, p=marg_b)
    ret[:,1] = beta[b_idxs]
    for i in xrange(size):
        ib = b_idxs[i]
        b_given_a = grid[ib]
        ia = np.random.choice(rng, p=b_given_a/marg_a[ib])
        ret[i,0] = alpha[ia]
    return ret


def sample_median(fname, nu, size=1000):
    data = startsites(fname)
    ss = suff_stats(data)
    pmode_a, pmode_b = post_mode(ss, nu)
    gl = grid_limits(pmode_a, pmode_b, ss.n, nu)
    alphas, betas, grid = post_grid(gl, ss, nu, size)
    samples = post_sample(alphas, betas, grid, size)
    return np.median(samples, axis=0)


firstlast = np.array([0,-1])
def _cred_int(density, lppf, uppf):
    cs = np.cumsum(density)
    ret = np.where((cs <= uppf) & (cs >= lppf))[0]
    if len(ret) < 1:
        mid = len(density)/2
        return np.array([mid-1, mid])
    return ret[firstlast]


def post_ci(alphas, betas, density_grid, lppf=0.025, uppf=0.975):
    idxs_a = _cred_int(density_grid.sum(axis=1), lppf, uppf)
    idxs_b = _cred_int(density_grid.sum(axis=0), lppf, uppf)
    return alphas[idxs_a], betas[idxs_b]
    

def cred_interval(fname, nu, size=1000, lppf=0.025, uppf=0.975):
    data = startsites(fname)
    ss = suff_stats(data)
    pmode_a, pmode_b = post_mode(ss, nu)
    gl = grid_limits(pmode_a, pmode_b, ss.n, nu)
    alphas, betas, grid = post_grid(gl, ss, nu, size)
    ci = post_ci(alphas, betas, grid, lppf, uppf)
    return ci
    
# nonspurious region; if both alpha and beta are within this region, I
# consider the model "uniform"
nr = (0.9, 1.1) # null region
betw_nr = lambda reg: reg[0] < nr[0] < reg[1] or reg[0] < nr[1] < reg[1]
def decide_spurious(alpha_ci, beta_ci):
    return not betw_nr(alpha_ci) and not betw_nr(beta_ci)
    

def alignments_are_spurious(fname, nu, size=1000, lppfs=(0.025,),
                            uppfs=(0.975,)):
    data = startsites(fname)
    ss = suff_stats(data)
    pmode_a, pmode_b = post_mode(ss, nu)
    gl = grid_limits(pmode_a, pmode_b, ss.n, nu)
    if not (gl[0] < 1 < gl[1] and gl[2] < 1 < gl[3]):
        # shortcut. if alpha grid bounds and beta grid bounds don't
        # both contain 1, then there's no wide enough credible
        # interval that can contain alpha=1 or beta=1; so, we say it's
        # spurious
        for _ in uppfs:
            yield True
    else:
        alphas, betas, grid = post_grid(gl, ss, nu, size)
        for lppf, uppf in zip(lppfs, uppfs):
            alpha_ci, beta_ci = post_ci(alphas, betas, grid, lppf, uppf)
            if decide_spurious(alpha_ci, beta_ci):
                yield True
            else:
                yield False


def get_ci_widths(size):
    return np.linspace(1./size, 0.5-(2./size))

def get_ci_ranges(size):
    widths = get_ci_widths(size)
    return {
        "lppfs": widths,
        "uppfs": 1-widths
    }
    

def _worker((fname, nu, size)):
    try:
        verdicts = list(alignments_are_spurious(fname, float(nu), int(size),
                                                **get_ci_ranges(size)))
    except KeyboardInterrupt:
        raise Exception("KeyboardInterrupt")
    except Exception as e:
        return fname, [str(e)]
    else:
        return fname, verdicts


def main(data_dir, nu, output_file, size=1000):
    nu = float(nu)
    size = int(size)
    if size % 2 == 0: 
        # we want size to be odd, so in case of a really narrow
        # credible region, we can choose the middle bin
        size += 1
    def _args():
        for fname in os.listdir(data_dir):
            if not fname.endswith(".json"):
                continue
            fname = os.path.join(data_dir, fname)
            yield fname, nu, size

    p = Pool(8)
    try:
        data = p.map(_worker, _args())
    except Exception:
        p.terminate()
    else:
        p.close()
    finally:
        p.join()

    with open(output_file, 'w') as f:
        json.dump({"data": data, "ci_widths": list(get_ci_widths(int(size)))}, f)
                

if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
