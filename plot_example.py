import json
from itertools import chain

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta

def startsites(data_fname):
    with open(data_fname, 'r') as f:
        data = json.load(f)
    return np.array([
        float(l[2]+1)/l[-1] for l in
        chain.from_iterable(data['data'].itervalues())
    ])
    

s_fname = "aggregated/UniRef50_U1LSV0[320:342]-from-UniRef50_UPI0004F02273[320:342]_miss-0_spurious_hit-23_hit-0_spurious_miss-0.json"
t_fname = "aggregated/UniRef50_A0A0F7SLU9_miss-0_spurious_hit-0_hit-756_spurious_miss-0.json"
t_data = startsites(t_fname)
s_data = startsites(s_fname)
ss_t, ss_s = map(suff_stats, (t_data, s_data))
fig, (ax1, ax2) = plt.subplots(2, sharex=True)

nus = (0.1, 0.5, 1, 2, 20)

h,b = np.histogram(s_data, bins=10)
ax1.set_xlim((0.,1.))
ax1.scatter(b[:-1], h)
x = np.linspace(0,1,100)
for nu in nus:
    a, b = post_mode(ss_s, nu)
    ax1.plot(x, beta(a,b).pdf(x),
             label=r"$\nu$=%r $\alpha$=%1.2f $\beta$=%1.2f"%(nu, a, b))
ax1.set_title("Spurious")
ax1.set_ylabel("Density")
ax1.set_xlabel("Normalized mapping start site location")
ax1.legend(ncol=3, loc="lower right", prop=dict(size=11))

h,b = np.histogram(t_data, bins=10)
ax2.set_xlim((0.,1.))
ax2.scatter(b[:-1], h/70.)
x = np.linspace(0,1,100)
for nu in nus:
    a, b = post_mode(ss_t, nu)
    ax2.plot(x, beta(a,b).pdf(x),
             label=r"$\nu$=%r $\alpha$=%1.2f $\beta$=%1.2f"%(nu, a, b))
ax2.set_title("Nonspurious")
ax2.set_ylabel("Density")
ax2.set_xlabel("Normalized mapping start site location")
ax2.legend(ncol=3, prop=dict(size=11))
plt.tight_layout()
plt.show()

