#!/usr/bin/env python

max_contam_size=33
min_contam_size=20

import sys
import random
from itertools import count

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

bern_50 = lambda: 0 if random.random() < 0.5 else 1
swap = lambda a, b: (b, a)
def random_startstop(maxlen, maxsize=max_contam_size, minsize=min_contam_size):
    start = random.randint(0, maxlen-maxsize)
    r = random.randint(minsize, maxsize)
    while True:
        stop = start + r if bern_50() else start - r
        if stop > 0:
            break
    if start > stop:
        return swap(start, stop)
    return start, stop

def contaminate(base, contam, count=1):
    maxlen = len(min(base, contam, key=len))
    newseq = list(base.seq)
    newdesc = ""
    for _ in xrange(count):
        start, stop = random_startstop(maxlen)
        contam_msg = "[{i}:{j}]-from-{fr}[{i}:{j}]"
        newdesc += contam_msg.format(to=base.id, fr=contam.id,
                                     i=start, j=stop)
        newseq[start:stop] = contam.seq[start:stop]
    return SeqRecord(Seq("".join(newseq)), id=base.id+newdesc,
                     description = newdesc)

def can_contaminate(seq):
    return len(seq)*.4 > max_contam_size

def semirandom_choice(choices):
    while True:
        choice = random.choice(choices)
        if can_contaminate(choice):
            return choice

def main(base_fname, contams_fname):
    contams = list(SeqIO.parse(contams_fname, 'fasta'))
    def _munged():
        for seq in SeqIO.parse(base_fname, 'fasta'):
            yield contaminate(seq, semirandom_choice(contams))
    SeqIO.write(_munged(), sys.stdout, 'fasta')
    

if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
